﻿using System;
using System.Collections.Generic;
using Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TemplateTest;

namespace TemplateTest.Controllers
{
    public class ProductsController : Controller
    {
        UnitOfWork uow = new UnitOfWork();
        string title = "Product";

        public ActionResult Index()
        {
			ViewBag.Title = title;
            return View(uow.ProductRepository.Get());
        }

        public ActionResult Create()
        {
			ViewBag.Title = title;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,ProductName,ProductDescription,ProductPrice")] Product product)
        {
			ViewBag.Title = title;
            if (ModelState.IsValid)
            {
                uow.ProductRepository.Add(product);
                uow.Save();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        public ActionResult Edit(int? id)
        {
			ViewBag.Title = title;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = uow.ProductRepository.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProductId,ProductName,ProductDescription,ProductPrice")] Product product)
        {
			ViewBag.Title = title;
            if (ModelState.IsValid)
            {
                uow.ProductRepository.Update(product);
                uow.Save();

                return RedirectToAction("Index");
            }
            return View(product);
        }

        public ActionResult Delete(int id)
        {
			ViewBag.Title = title;
            Product product = uow.ProductRepository.Find(id);
            uow.ProductRepository.Remove(product);
            uow.Save();
            return RedirectToAction("Index");
        }
    }
}
