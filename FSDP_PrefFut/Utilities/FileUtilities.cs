﻿using System.IO;
using System.Web;

namespace Utilities
{
    public class FileUtilities
    {
        public static void UploadFile(string savePath, string fileName, HttpPostedFileBase file)
        {
            file.SaveAs(savePath + fileName);
        }

        public static void Delete(string path)
        {
            // Get info about the specified file
            FileInfo file = new FileInfo(path);

            // Check if the file exists
            if (file.Exists)
            {
                // If so delete the file
                file.Delete();
            }
        }
    }
}
