﻿namespace Utilities
{
    public static class VideoUtilities
    {
        public static string ParseYouTubeUrl(string url)
        {
            var v = url.IndexOf("v=");
            string videoId;

            if (v > 0)
            {
                var amp = url.IndexOf("&", v);

                // if the video id is the last value in the url
                if (amp == -1)
                {
                    videoId = url.Substring(v + 2);
                    // if there are other parameters after the video id in the url
                }
                else
                {
                    videoId = url.Substring(v + 2, amp - (v + 2));
                }
            }
            else
            {
                videoId = url;
            }

            return videoId;
        }
    }
}

