﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Repositories
{
    public partial class LocationRepository : GenericRepository<Location>
    {
        public LocationRepository(DbContext context) : base(context)
        {
        }
    }

    public partial class ReservationRepository : GenericRepository<Reservation>
    {
        public ReservationRepository(DbContext context) : base(context)
        {
        }
    }

    public partial class StudentDocRepository : GenericRepository<StudentDoc>
    {
        public StudentDocRepository(DbContext context) : base(context)
        {
        }
    }

    public partial class UserDetailRepository : GenericRepository<UserDetail>
    {
        public UserDetailRepository(DbContext context) : base(context)
        {
        }
    }

}
