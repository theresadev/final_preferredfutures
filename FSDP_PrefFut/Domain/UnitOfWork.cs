﻿using Data;
using Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public interface IUnitOfWork : IDisposable
    {
        void Save();
    }

    public class UnitOfWork : IUnitOfWork
    {
        DbContext context = new PreferredFuturesEntities();

        private LocationRepository _locationRepository;
        public LocationRepository LocationRepository
        {
            get
            {
                if (this._locationRepository == null)
                {
                    this._locationRepository = new LocationRepository(context);
                }

                return _locationRepository;
            }
        }

        private ReservationRepository _reservationRepository;
        public ReservationRepository ReservationRepository
        {
            get
            {
                if (this._reservationRepository == null)
                {
                    this._reservationRepository = new ReservationRepository(context);
                }

                return _reservationRepository;
            }
        }

        private StudentDocRepository _studentDocRepository;
        public StudentDocRepository StudentDocRepository
        {
            get
            {
                if (this._studentDocRepository == null)
                {
                    this._studentDocRepository = new StudentDocRepository(context);
                }

                return _studentDocRepository;
            }
        }

        private UserDetailRepository _userDetailRepository;
        public UserDetailRepository UserDetailRepository
        {
            get
            {
                if (this._userDetailRepository == null)
                {
                    this._userDetailRepository = new UserDetailRepository(context);
                }

                return _userDetailRepository;
            }
        }


        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
