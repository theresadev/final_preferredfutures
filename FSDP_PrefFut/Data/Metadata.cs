﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;//added ctrl. on Display
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
    [MetadataType(typeof(LocationMetadata))]
    public partial class Location { }
    
    public class LocationMetadata    
    {
            public int LocationID { get; set; }

        [Display(Name = "Location Name")]
        [Required(ErrorMessage = "* Required")]
        public string LocationName { get; set; }

        [Display(Name = "Street Address")]
        [Required(ErrorMessage = "* Required")]
        public string Address { get; set; }

        [Display(Name = "City")]
        [Required(ErrorMessage = "* Required")]
        public string City { get; set; }

        [Display(Name = "State")]
        [Required(ErrorMessage = "* Required")]
        public string State { get; set; }

        [Display(Name = "Zip Code")]
        [Required(ErrorMessage = "* Required")]
        public string ZipCode { get; set; }

        [Display(Name = "Maximum Students Allowed")]
        [Required(ErrorMessage = "* Required")]
        public byte ReservationLimit { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Instructor")]
        public string TeacherID { get; set; }

        [Display(Name = "Document Requirements")]
        [Required(ErrorMessage = "* Required")]
        public string DocRequirements { get; set; }

        [Display(Name = "Additional Comments")]
        public string Comments { get; set; }

        }

    [MetadataType(typeof(ReservationMetadata))]
    public partial class Reseration { }

    public class ReservationMetadata
    {
        public int ReservationID { get; set; }

        [Display(Name = "Location ID")]
        [Required(ErrorMessage = "* Required")]
        public int LocationID { get; set; }

        [Display(Name = "Reservation Date")]
        [Required(ErrorMessage = "* Required")]
        public System.DateTime ReservationDate { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Teacher")]
        public string TeacherID { get; private set; }

        [Display(Name = "Document ID")]
        [Required(ErrorMessage = "* Required")]
        public int StudentDocID { get; set; }

    }

    [MetadataType(typeof(StudentDocMetadata))]
    public partial class StudentDoc { }

    public class StudentDocMetadata
    {
        public int StudentDocID { get; set; }

        [Display(Name = "EDL")]
        //[Required(ErrorMessage = "* Required")] ---- commented out to allow for testing ... 
        public string DocName { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Student")]
        public string StudentID { get; private set; }

        [Display(Name = "Photo")]
        public string StudentPhoto { get; set; }

        [Display(Name = "Special Notes")]
        public string SpecialNotes { get; set; }

        [Display(Name = "Active Student?")]
        [Required(ErrorMessage = "* Required")]
        public bool IsActive { get; private set; }

        [Display(Name = "Date Added")]
        [Required(ErrorMessage = "* Required")]
        public System.DateTime DateAdded { get; set; }
    }

        [MetadataType(typeof(UserDetailMetadata))]
        public partial class UserDetail { }

        public class UserDetailMetadata
    {
        public string UserID { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "* Required")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "* Required")]
        public string LastName { get; set; }
    }

}

