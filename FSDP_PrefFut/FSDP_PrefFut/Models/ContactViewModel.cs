﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;//added ctrl. on Required(
using System.Linq;
using System.Web;

namespace FSDP_PrefFut.Models
{
    public class ContactViewModel
    {
        [Required(ErrorMessage = "Name Required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email Required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone Number Required")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Message Required")]
        public string Message { get; set; }

    }
}