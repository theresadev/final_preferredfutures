﻿using System.Web;
using System.Web.Mvc;

namespace FSDP_PrefFut
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
