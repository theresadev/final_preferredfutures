﻿using System;
using System.Collections.Generic;
using Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;//added ctrl. on unitofwork
using Microsoft.AspNet.Identity;//added for identity
//using System.Data.Entity;//added for enttities

namespace FSDP_PrefFut.Controllers
{
    [Authorize(Roles = "Admin, Teacher, Student")]
    public class ReservationsController : Controller
    {
        //UnitOfWork uow = new UnitOfWork();

        PreferredFuturesEntities db = new PreferredFuturesEntities();
        string title = "Class Reservation";

        public ActionResult Index()
        {
            //to display name instead of GUID
            List<UserDetail> deets = db.UserDetails.ToList();
            ViewBag.userDeets = deets;

            ViewBag.Title = title;
            //var reservations = new List<Data.Reseration>();
            //var studentDocID = db.StudentDocs.ToList();
            var reservations = db.Reservations.ToList();
            var userId = User.Identity.GetUserId();
            var studentDoc = db.StudentDocs.Where(s => s.StudentID == userId);

            //var res = db.Reservations.Where(rd => rd.ReservationDate == reservation.ReservationDate && rd.LocationID == reservation.LocationID).Single();
            //var studentID = db.Reservations.Where(s => s.StudentDocID == db.StudentDocs.StudentDocID && s.StudentDoc == studentDoc.StudentID).Single();

            //var studentID = db.StudentDocs.ToList().Where(s => s.StudentID == userId).ToList();
            if (User.IsInRole("Admin"))
            {
                reservations = db.Reservations.ToList();
            }
            else
                        if (User.IsInRole("Teacher"))
            {
                reservations = db.Reservations.ToList();
            }
            else
            {
                List<Reservation> stuRes = new List<Reservation>();
                var SDID = db.StudentDocs.Where(x => x.StudentID == userId);
                foreach (var r in db.Reservations)
                {
                    foreach (var s in SDID)
                    {
                        if (r.StudentDocID == s.StudentDocID)
                        {
                            stuRes.Add(r);
                        }
                    }
                }
                return View(stuRes.ToList());
                //var userId = User.Identity.GetUserId();
                //reservations = db.Reservations.ToList().Where(s => s.StudentID == userId).ToList();
                //reservations = db.Reservations.ToList().Where(s.studentID == userId);
            }
            return View(reservations.ToList());
        }


        public ActionResult Create()
        {
			ViewBag.Title = title;
            var userId = User.Identity.GetUserId();
            var studentDoc = db.StudentDocs.Where(s => s.StudentID == userId);
            string currentUserID = User.Identity.GetUserId();
            //ViewBag.TeacherID = new SelectList(db.Locations.Where(tID => tID.UserID == ))
            ViewBag.LocationID = new SelectList(db.Locations.ToList(), "LocationID", "LocationName");
            ViewBag.StudentDocID = new SelectList(db.StudentDocs.Where(s => s.StudentID == userId), "StudentDocID", "DocName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReservationID,LocationID,ReservationDate,TeacherID,StudentDocID")] Reservation reservation)
        {
			ViewBag.Title = title;
            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            if (ModelState.IsValid)
            {
                //Location location = db.Locations.Where(l => l.LocationID == reservation.LocationID).Single();
                var res = db.Reservations.Where(rd => rd.ReservationDate == reservation.ReservationDate && rd.LocationID == reservation.LocationID).SingleOrDefault();
                //if (location.ReservationLimit <= location.ReservationLimit)
                if (res == null)
                {
                    db.Reservations.Add(reservation);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                ViewBag.ErrorMessage = "The class size limit has been reached.  Please select a different date.";

                //db.Reservations.Add(reservation);
                //db.SaveChanges();
            }

            ViewBag.LocationID = new SelectList(db.Locations.ToList(), "LocationID", "LocationName");
            ViewBag.StudentDocID = new SelectList(db.StudentDocs.ToList(), "StudentDocID", "DocName");
            return View(reservation);
        }

        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Edit(int? id)
        {
            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            ViewBag.Title = title;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservation reservation = db.Reservations.Find(id);
            if (reservation == null)
            {
                return HttpNotFound();
            }
            ViewBag.LocationID = new SelectList(db.Locations.ToList(), "LocationID", "LocationName");
            ViewBag.StudentDocID = new SelectList(db.StudentDocs.ToList(), "StudentDocID", "DocName");
            return View(reservation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Edit([Bind(Include = "ReservationID,LocationID,ReservationDate,TeacherID,StudentDocID")] Reservation reservation)
        {

            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            ViewBag.Title = title;
            var userId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                reservation.TeacherID = userId;

                db.Entry(reservation).State = EntityState.Modified; ;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            ViewBag.LocationID = new SelectList(db.Locations.ToList(), "LocationID", "LocationName");
            ViewBag.StudentDocID = new SelectList(db.StudentDocs.ToList(), "StudentDocs", "DocName");
            return View(reservation);
        }

        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Delete(int id)
        {
			ViewBag.Title = title;
            Reservation reservation = db.Reservations.Find(id);
            db.Reservations.Remove(reservation);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
