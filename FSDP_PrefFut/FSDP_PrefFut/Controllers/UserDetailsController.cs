﻿using System;
using System.Collections.Generic;
using Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;//added ctrl. on unitofwork
using Microsoft.AspNet.Identity;//added for identity

namespace FSDP_PrefFut.Controllers
{
    public class UserDetailsController : Controller
    {
        UnitOfWork uow = new UnitOfWork();
        string title = "UserDetail";

        public ActionResult Index()
        {
			ViewBag.Title = title;
            return View(uow.UserDetailRepository.Get());
        }

        public ActionResult Create()
        {
			ViewBag.Title = title;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UserID,FirstName,LastName")] UserDetail userDetail)
        {
			ViewBag.Title = title;
            if (ModelState.IsValid)
            {
                uow.UserDetailRepository.Add(userDetail);
                uow.Save();
                return RedirectToAction("Index");
            }

            return View(userDetail);
        }

        public ActionResult Edit(string id)
        {
			ViewBag.Title = title;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserDetail userDetail = uow.UserDetailRepository.Find(id);
            if (userDetail == null)
            {
                return HttpNotFound();
            }
            return View(userDetail);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserID,FirstName,LastName")] UserDetail userDetail)
        {
			ViewBag.Title = title;
            if (ModelState.IsValid)
            {
                uow.UserDetailRepository.Update(userDetail);
                uow.Save();

                return RedirectToAction("Index");
            }
            return View(userDetail);
        }

        public ActionResult Delete(string id)
        {
			ViewBag.Title = title;
            UserDetail userDetail = uow.UserDetailRepository.Find(id);
            uow.UserDetailRepository.Remove(userDetail);
            uow.Save();
            return RedirectToAction("Index");
        }
    }
}
