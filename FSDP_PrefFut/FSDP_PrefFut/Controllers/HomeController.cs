﻿using FSDP_PrefFut.Models;//added ctrl. on contactviewmodel
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;//added ctl. on NetworkCredential
using System.Net.Mail;//added ctrl. on MailMessage
using System.Web;
using System.Web.Mvc;

namespace FSDP_PrefFut.Controllers
{
    public class HomeController : Controller
    {
        public object Name { get; private set; }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Contact(ContactViewModel contact)
        {
            if (ModelState.IsValid)
            {
                //body of email:
                string body = string.Format("Name: {0}<br/>Email: {1}<br/>Subject: {2}<br/>Message:<br/> {3}",
                    contact.Name,
                contact.Email,
                contact.Phone,
                contact.Message);

                //create & configure email message...
                MailMessage msg = new MailMessage(
                    "no-reply@preferredfutures.com",//from address
                    "t.johnstone1194@gmail.com",//to address
                    contact.Phone,
                    body);

                //configure MailMessage object...
                msg.IsBodyHtml = true;//treats text as HTML

                //create & configure SMTP client
                //SmtpClient client = new SmtpClient("mail.preferredfutures.com");
                //client.Credentials = new NetworkCredential("no-reply@preferredfutures.com", "P@ssw0rd!");

                SmtpClient client = new SmtpClient("mail.theresadev.com");
                client.Credentials = new NetworkCredential("no-reply@theresadev.com", "P@ssw0rd!");

                //using directive
                using (client)
                {
                    try
                    {
                        client.Send(msg);
                    }
                    catch
                    {
                        ViewBag.ErrorMessage = "There was a problem sending your email.  Please try again later.  Sorry for the inconvenience.";
                        return View(contact);
                    }
                }
                return View("ContactConfirmation", contact);
            }
            return View(contact);
        }
    }
}
