﻿using System;
using System.Collections.Generic;
using Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;//added ctrl. on unitofwork
using Microsoft.AspNet.Identity;//added for identity
using System.IO;//added for image 
using System.Drawing;//added for image 
using Utilities;//added for image 


namespace FSDP_PrefFut.Controllers
{
    [Authorize]
    public class StudentDocsController : Controller
    {
        //UnitOfWork uow = new UnitOfWork();
        PreferredFuturesEntities db = new PreferredFuturesEntities();
        string title = "Student Documents";

        public ActionResult Index()
        {
            //to display name instead of GUID
            List<UserDetail> deets = db.UserDetails.ToList();
            ViewBag.userDeets = deets;


            ViewBag.Title = title;
            var studentDocs = new List<Data.StudentDoc>();
            if (User.IsInRole("Admin"))
            {
                studentDocs = db.StudentDocs.ToList().ToList();
            }
            else
            {
                var userId = User.Identity.GetUserId();
                studentDocs = db.StudentDocs.ToList().Where(s => s.StudentID == userId).ToList();
            }
            return View(studentDocs);
        }

        public ActionResult Create()
        {
			ViewBag.Title = title;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "StudentDocID,DocName,StudentID,StudentPhoto,SpecialNotes,IsActive,DateAdded")] StudentDoc studentDoc, HttpPostedFileBase edlFileUpload, HttpPostedFileBase userImageUpload)
        {
			ViewBag.Title = title;

            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            //file and image upload
            FileUtilities.UploadFile(Server.MapPath("~/Content/images/"), edlFileUpload.FileName, edlFileUpload);

            FileUtilities.UploadFile(Server.MapPath("~/Content/images/"), userImageUpload.FileName, userImageUpload);
            //var edlFileUpload = FileUtilities.UploadFile(studentDoc.DocName);
            //var userImageUpload = ImageUtilities.ResizeImage(studentDoc.StudentPhoto);

            var dateAdded = DateTime.Now;
            var dateOnlyString = dateAdded.ToShortDateString();
            var userId = User.Identity.GetUserId();
            var studentName = db.StudentDocs.ToList();
            

            if (ModelState.IsValid)
            {

                studentDoc.DocName = edlFileUpload.FileName;
                studentDoc.StudentPhoto = userImageUpload.FileName;
                studentDoc.StudentID = userId;
                studentDoc.DateAdded = dateAdded;

                db.StudentDocs.Add(studentDoc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(studentDoc);
        }

        public ActionResult Edit(int? id)
        {
            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            ViewBag.Title = title;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StudentDoc studentDoc = db.StudentDocs.Find(id);
            if (studentDoc == null)
            {
                return HttpNotFound();
            }
            return View(studentDoc);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "StudentDocID,DocName,StudentID,StudentPhoto,SpecialNotes,IsActive,DateAdded")] StudentDoc studentDoc, HttpPostedFileBase edlFileUpload, HttpPostedFileBase userImageUpload)
        {

            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;


            ViewBag.Title = title;
            //file and image upload

            FileUtilities.UploadFile(Server.MapPath("~/Content/images/"), edlFileUpload.FileName, edlFileUpload);
            FileUtilities.UploadFile(Server.MapPath("~/Content/images/"), userImageUpload.FileName, userImageUpload);
            //var edlFileUpload = FileUtilities.UploadFile(studentDoc.DocName);
            //var userImageUpload = ImageUtilities.ResizeImage(studentDoc.StudentPhoto);

            var dateAdded = DateTime.Now;
            var dateOnlyString = dateAdded.ToShortDateString();
            var userId = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {

                studentDoc.DocName = edlFileUpload.FileName;
                studentDoc.StudentPhoto = userImageUpload.FileName;
                studentDoc.StudentID = userId;
                studentDoc.DateAdded = dateAdded;

                db.Entry(studentDoc).State = EntityState.Modified; ;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(studentDoc);
        }

        //[Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
			ViewBag.Title = title;
            StudentDoc studentDoc = db.StudentDocs.Find(id);
            db.StudentDocs.Remove(studentDoc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
