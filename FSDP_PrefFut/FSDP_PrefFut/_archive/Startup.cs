﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FSDP_PrefFut.Startup))]
namespace FSDP_PrefFut
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
