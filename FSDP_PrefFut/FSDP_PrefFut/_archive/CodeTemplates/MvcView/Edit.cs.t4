﻿<#@ template language="C#" HostSpecific="True" #>
<#@ output extension=".cshtml" #>
<#@ include file="Imports.include.t4" #>
@model <#= ViewDataTypeName #>
<#
// "form-control" attribute is only supported for all EditorFor() in System.Web.Mvc 5.1.0.0 or later versions, except for checkbox, which uses a div in Bootstrap
string boolType = "System.Boolean";
Version requiredMvcVersion = new Version("5.1.0.0");
bool isControlHtmlAttributesSupported = MvcVersion >= requiredMvcVersion;
// The following chained if-statement outputs the file header code and markup for a partial view, a view using a layout page, or a regular view.
if(IsPartialView) {
#>

<#
} else if(IsLayoutPageSelected) {
#>

@{
<#
if (!String.IsNullOrEmpty(LayoutPageFile)) {
#>
    Layout = "<#= LayoutPageFile#>";
<#
}
#>
}

@section styles {<style>.control-label {font-weight:bold;color:black;}input.detailDisplay {background-color:white !important;box-shadow:none;border:0;color:black;}.editForm input,.editForm select {color:black;background-color:rgba(255, 216, 0, .3);}</style>}

<#
} else {
#>

@{
    Layout = null;
}

<!DOCTYPE html>

<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <title><#= ViewName #></title>
</head>
<body>
<#
    PushIndent("    ");
}
#>
<#
if (ReferenceScriptLibraries) {
#>
<#
    if (!IsLayoutPageSelected && IsBundleConfigPresent) {
#>
@Scripts.Render("~/bundles/jquery")
@Scripts.Render("~/bundles/jqueryval")

<#
    }
#>
<#
    else if (!IsLayoutPageSelected) {
#>
<script src="~/Scripts/jquery-<#= JQueryVersion #>.min.js"></script>
<script src="~/Scripts/jquery.validate.min.js"></script>
<script src="~/Scripts/jquery.validate.unobtrusive.min.js"></script>

<#
    }
#>

<#
}
#>
@using (Html.BeginForm())
{
    @Html.AntiForgeryToken()
    
    <div class="form-horizontal">
	    <h2><span class="editForm">Edit </span>@ViewBag.Title<span class="detailDisplay"> Details</span></h2>
        <hr />

<#
foreach (PropertyMetadata property in ModelMetadata.Properties) {
    if (property.Scaffold && !property.IsAssociation) {
        if (property.IsPrimaryKey) {
#>
        @Html.HiddenFor(model => model.<#= property.PropertyName #>)

<#
        } else if (!property.IsReadOnly) {
#>
        <div class="form-group">

            @Html.LabelFor(model => model.<#= property.PropertyName #>, htmlAttributes: new { @class = "control-label col-md-2" })

            <div class="col-md-10">
				<input type="text" name="ro<#= property.PropertyName #>" value="@Model.<#= property.PropertyName #>" class="detailDisplay form-control" readonly />
                <div class="editForm">
<#
            bool isCheckbox = property.TypeName.Equals(boolType);
            if (property.IsForeignKey) {
#>
<# 
            if (isControlHtmlAttributesSupported) {
#>
                @Html.DropDownList("<#= property.PropertyName #>", null, htmlAttributes: new { @class = "form-control" })
<#
            } else {
#>
                @Html.DropDownList("<#= property.PropertyName #>", String.Empty)
<#
            }
#>
<#
            } else  if (isControlHtmlAttributesSupported) {
                if (isCheckbox) {
#>
                <div class="checkbox">
<#
                    PushIndent("    ");
#>
                @Html.EditorFor(model => model.<#= property.PropertyName #>)
<#
                } else if (property.IsEnum && !property.IsEnumFlags) {
#>
                @Html.EnumDropDownListFor(model => model.<#= property.PropertyName #>, htmlAttributes: new { @class = "form-control" })
<#
                } else {
#>
                @Html.EditorFor(model => model.<#= property.PropertyName #>, new { htmlAttributes = new { @class = "form-control" } })
<#
                }
            } else {
#>
                @Html.EditorFor(model => model.<#= property.PropertyName #>)
<#
            }
#>
<# 
            if (isControlHtmlAttributesSupported) {
#>
                @Html.ValidationMessageFor(model => model.<#= property.PropertyName #>, "", new { @class = "text-danger" })
<#        
            } else {
#>
                @Html.ValidationMessageFor(model => model.<#= property.PropertyName #>)
<#      
            }
#>
<#
            if (isCheckbox && isControlHtmlAttributesSupported) {
                PopIndent();
#>
                </div>
<#
            }
#>
				</div>
			</div>
		</div>
<#
        }
    }
}
#>
		<div class="form-group editForm">
			<div class="col-md-offset-2 col-md-10">
				<input type="submit" value="Save" class="submitBtn btn btn-info" />
			</div>
		</div>
	</div>
}

<div>
    <a href="@Url.Action("Index")" class = "btn btn-info" >Back to List</a> 
	<span id="showEdit" class="detailDisplay btn btn-warning">Edit Record</span> 
    <span id="showDetails" class="editForm btn btn-success">View Details</span>
</div>

@section Scripts {
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validation-unobtrusive/3.2.10/jquery.validate.unobtrusive.min.js"></script>
	<script>
        $(".editForm").hide();
        $(".submitBtn").css('visibility', 'hidden');
        $("#showEdit").click(function () {
            $(".editForm").show();
            $(".detailDisplay").hide();
            $(".submitBtn").css('visibility', 'visible');
        });
        $("#showDetails").click(function () {
            $(".editForm").hide();
            $(".detailDisplay").show();
            $(".submitBtn").css('visibility', 'hidden');
        });
    </script>
}
<#
// The following code closes the tag used in the case of a view using a layout page and the body and html tags in the case of a regular view page
#>
<#
if(!IsPartialView && !IsLayoutPageSelected) {
    ClearIndent();
#>
</body>
</html>
<#
}
#>
<#@ include file="ModelMetadataFunctions.cs.include.t4" #>