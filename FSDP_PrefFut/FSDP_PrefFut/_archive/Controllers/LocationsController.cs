﻿using System;
using System.Collections.Generic;
using Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Domain;//added ctrl. on unitofwork
using Microsoft.AspNet.Identity;//added for identity

namespace FSDP_PrefFut.Controllers
{
[Authorize]
    public class LocationsController : Controller
    {
        //UnitOfWork uow = new UnitOfWork();
        PreferredFuturesEntities db = new PreferredFuturesEntities();

        string title = "Classes & Locations";

        [AllowAnonymous]
        public ActionResult Index()
        {
			ViewBag.Title = title;

            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;


            //GET: Locations ...... 
            var locations = db.Locations.ToList();
            return View(locations.ToList());
        }

        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Create()
        {
            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;

            ViewBag.Title = title;
            ViewBag.LocationID = new SelectList(db.Locations.ToList(), "LocationID", "LocationName");
            ViewBag.StudentDocID = new SelectList(db.StudentDocs, "StudentDocID", "DocName");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Create([Bind(Include = "LocationID,LocationName,Address,City,State,ZipCode,ReservationLimit,TeacherID,DocRequirements,Comments")] Location location)
        {
			ViewBag.Title = title;

            var teacherID = User.Identity.GetUserId();
            var userFirstName = User.Identity.GetUserId();

            if (ModelState.IsValid)
            {
                location.TeacherID = userFirstName;

                db.Locations.Add(location);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(location);
        }

        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Edit(int? id)
        {
            ////to display name instead of GUID
            //List<UserDetail> deets = db.UserDetails.ToList();
            //ViewBag.userDeets = deets;


            ViewBag.Title = title;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Location location = db.Locations.Find(id);
            if (location == null)
            {
                return HttpNotFound();
            }
            return View(location);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Teacher")]
        public ActionResult Edit([Bind(Include = "LocationID,LocationName,Address,City,State,ZipCode,ReservationLimit,TeacherID,DocRequirements,Comments")] Location location)
        {
			ViewBag.Title = title;
            if (ModelState.IsValid)
            {
                db.Entry(location).State = EntityState.Modified; ;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            return View(location);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
			ViewBag.Title = title;
            Location location = db.Locations.Find(id);
            db.Locations.Remove(location);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
