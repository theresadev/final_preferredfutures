IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_StudentDocs]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations] DROP CONSTRAINT [FK_Reservations_StudentDocs]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations] DROP CONSTRAINT [FK_Reservations_Locations]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] DROP CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] DROP CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] DROP CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_StudentDocs_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StudentDocs] DROP CONSTRAINT [DF_StudentDocs_DateAdded]
END

GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
DROP TABLE [dbo].[UserDetails]
GO
/****** Object:  Table [dbo].[StudentDocs]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentDocs]') AND type in (N'U'))
DROP TABLE [dbo].[StudentDocs]
GO
/****** Object:  Table [dbo].[Reservations]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reservations]') AND type in (N'U'))
DROP TABLE [dbo].[Reservations]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
DROP TABLE [dbo].[Locations]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUsers]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserRoles]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserLogins]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetUserClaims]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
DROP TABLE [dbo].[AspNetRoles]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 3/27/2019 8:52:36 AM ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
DROP TABLE [dbo].[__MigrationHistory]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[__MigrationHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AspNetUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Locations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Locations](
	[LocationID] [int] IDENTITY(1,1) NOT NULL,
	[LocationName] [varchar](60) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[State] [char](2) NOT NULL,
	[ZipCode] [char](5) NOT NULL,
	[ReservationLimit] [tinyint] NOT NULL,
	[TeacherID] [nvarchar](128) NOT NULL,
	[DocRequirements] [varchar](80) NOT NULL,
	[Comments] [varchar](300) NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[LocationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Reservations]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Reservations]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Reservations](
	[ReservationID] [int] IDENTITY(1,1) NOT NULL,
	[LocationID] [int] NOT NULL,
	[ReservationDate] [date] NOT NULL,
	[TeacherID] [nvarchar](128) NULL,
	[StudentDocID] [int] NOT NULL,
 CONSTRAINT [PK_Reservations] PRIMARY KEY CLUSTERED 
(
	[ReservationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[StudentDocs]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentDocs]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[StudentDocs](
	[StudentDocID] [int] IDENTITY(1,1) NOT NULL,
	[DocName] [varchar](80) NOT NULL,
	[StudentID] [nvarchar](128) NOT NULL,
	[StudentPhoto] [varchar](50) NULL,
	[SpecialNotes] [varchar](300) NULL,
	[IsActive] [bit] NOT NULL,
	[DateAdded] [date] NOT NULL,
 CONSTRAINT [PK_StudentDocs] PRIMARY KEY CLUSTERED 
(
	[StudentDocID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserDetails]    Script Date: 3/27/2019 8:52:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserDetails]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[UserDetails](
	[UserID] [nvarchar](128) NOT NULL,
	[FirstName] [varchar](50) NOT NULL,
	[LastName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_UserDetails] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201903191400126_InitialCreate', N'FSDP_PrefFut.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CDB6EE336107D2FD07F10F4D416A9954B77B10DEC16A913B7413717ACB345DF025AA21D61254A95A86C82A25FD6877E527FA14389BAF1A28BADD84EB1C02222876786C321391C0EFDEFDFFF8C7F7CF23DE31147B11B908979343A340D4CECC071C96A622674F9ED3BF3C71FBEFC627CE1F84FC66F39DD09A38396249E980F9486A79615DB0FD847F1C877ED288883251DD9816F2127B08E0F0FBFB78E8E2C0C10266019C6F84342A8EBE3F4033EA701B1714813E45D050EF6625E0E35F314D5B8463E8E4364E389399B9FDFDEDF4678394BE82823378D33CF4520CA1C7B4BD3408404145110F4F4638CE7340AC86A1E4201F2EE9E430C744BE4C59877E0B424EFDA97C363D617AB6C9843D9494C03BF27E0D109578E25365F4BC566A13C50DF05A8993EB35EA72A9C98970E4E8B3E041E284064783AF522463C31AF0A166771788DE9286F38CA206711C07D0EA24FA32AE281D1B9DD41614CC7A343F6EFC098261E4D223C2138A111F20E8CDB64E1B9F6AFF8F92EF884C9E4E468B13C79F7E62D724EDE7E874FDE547B0A7D05BA5A0114DD46418823900D2F8BFE9B86556F67890D8B6695369956C096605E98C6157A7A8FC98A3EC08C397E671A33F7093B790937AE8FC48569048D6894C0E775E27968E1E1A2DE6AE4C9FE6FE07AFCE6ED205CAFD1A3BB4A875EE00F13278279F5017B696DFCE086D9F4AA8DF73D279B4581CFBEEBF695D5DECF8324B25967022DC91D8A5698D6A51B5BA5F17632690635BC59E7A8FB6FDA4C52D9BC95A4AC43EBCC849CC5B667432EEFCBF2ED6C7167610883979A16D34893C12976AB91D0FCC0A81295C673D4D5780874EAFFBC165EF8C8F506580C3B70014764E9463E2E7AF95300A687486F996F511CC35AE0FC82E28706D1E1CF01449F633B89C044E714F9E18B73BB7D0808BE4EFC05B3FCEDF11A6C68EE3E073364D320BA20ACD5C678EF03FB5390D00BE29C238A3F523B07649F77AEDF1D601071CE6C1BC7F10C8C193BD300FCEC1CF092D093E3DE706C85DAB53332F590EBABBD11612DBDCF494B8F444D2179251A329567D224EAFB60E5926EA2E6A47A51338A565139595F511958374939A55ED094A055CE8C6A305F2F1DA1E19DBD1476FFBDBDCD366FDD5A5051E31C5648FC3326388265CCB94594E2889423D065DDD885B3900E1F63FAE27B53CAE937E42543B35A6B36A48BC0F0B32185DDFFD9908A09C58FAEC3BC920E47A09C18E03BD1AB4F57ED734E906CDBD3A1D6CD6D33DFCE1AA09B2E67711CD86E3A0B14C12F1EBAA8CB0F3E9CD11EC7C87A23C642A06360E82EDBF2A004FA668A467543CEB1872936CEEC2C383845B18D1C598DD021A78760F98EAA10AC8C89D485FB46E209968E23D608B143500C33D525549E162EB1DD1079AD5A125A76DCC258DF0B1E62CD390E31610C5B35D185B93A04C20428F80883D2A6A1B155B1B86643D478ADBA316F7361CB719722135BB1C916DF596397DC7F7B11C36CD6D8168CB359255D04D086F37661A0FCACD2D500C483CBBE19A87062D2182877A9B662A0758DEDC040EB2A7975069A1D51BB8EBF705EDD37F3AC1F94B7BFAD37AA6B07B659D3C79E9966E67B421B0A2D70249BE7F98255E227AA389C819CFC7C167357573411063EC7B41EB229FD5DA51F6A35838846D404581A5A0B28BF089480A409D543B83C96D7281DF7227AC0E671B74658BEF60BB0151B90B1AB17A21542FDB5A9689C9D4E1F45CF0A6B908CBCD361A182A3300871F1AA77BC835274715959315D7CE13EDE70A5637C301A14D4E2B96A94947766702DE5A6D9AE259543D6C725DB484B82FBA4D152DE99C1B5C46DB45D490AA7A0875BB0918AEA5BF840932D8F7414BB4D5137B6B244295E30B6341955E32B14862E595532AC788931CFD2ABA6DFCEFBA71DF9198665C78AECA342DA82130D22B4C2422DB00649676E14D37344D102B138CFD4F12532E5DEAA59FE7396D5ED531EC47C1FC8A9D9DF590BD5F57D6DB395BD110E32832EFACCA549E3E80A0350373758CA1BF250A408DD4F032FF189DEC3D2B7CE2EF0AAEDB31219616C09F24B1E94A42EC9CFADEBBED3C8C8B362A8512A3C98F5474A0FA1D377EE7F5635AEF349F5287988AA8AA20B5BED6CE474AE4CBFD112DDC4FE83D58AF032338BE7A6540178514F8C4A7A830456A9EB8E5ACF40A962D66BBA230A69265548A1AA8794D564929A90D58AB5F0341A555374E720A78F54D1E5DAEEC88A44922AB4A27A0D6C85CC625D775445AE49155851DD1DBB4C3C1157D13DDEBBB48797F537AFEC80BBD9EEA5C17899257198CDAF728F5F05AA14F7C4E237F512182FDF4B73D29EF2D637A72CB0B199396930F46B4FED0ABCBEF434DEDBEB316BF7DAB5E5BDE95E5F8FD7CF685FD434A4539E4852702F4E7BC2A96ECC4F58ED8F69A4235746621AB91A616B7F8E29F6478C6034FFC39B7A2E660B794E708588BBC431CD7239CCE3C3A363E139CEFE3C8DB1E2D8F1142754DDFB98FA986D212D8B3CA2C87E40919C24B1C1F39112548A3F5F12073F4DCC3FD356A7692883FD95161F1897F147E2FE9140C55D9460E32F39E9739874FAE673D69E3E7EE8AED5CBDFEFB3A607C64D0433E6D4381474B9CE08D79F44F492266BBA81346B3F9478BD13AAF60641892A4C88F59F1C2C5C3AC873835CCAAF7CF4F4755FD1944F0A3642543C1B180A6F1015EA9E05AC83A57D12E0C0274D9F04F4EBACFA89C03AA2699F07B8A43F98F838A0FB3294B7DCE156A338146D63494AF5DC9A5CBD51A6E5AEF72629077BA3892EE759F780DB20977A0DCB786569C883ED8E8A2CE3C1B07769DA2F9E5ABC2FD9C4659EC76E9388B79937DC7033F4BF4A17DE83043745C2CEEE9382B76D6BBA40EE9E6756F64BFDDD3363E3695CBB4FF0DDB6B1E9C2BC7B6E6CBDD278F7CCD676B57FEED8D23A6FA13B4FCA95F38B341732AA58705BD26D16388713FE220023C83CCAECADA43ACBAB2943B5856149A267AA4F2F13194B1347E22B5134B3EDD757BEE1377696D334B3D5246536F1E6EB7F236F4ED3CC5B93EAB88B746165B2A12A85BB651D6BCA837A4DE9C1B59EB464A3B7F9AC8DB7EBAF291B7810A5D4668FE68EF8F524FF0EA29221A74E8F645FF9BA17F6CECA2F2CC2FE1DBBAB1282FDDE22C1766DD72C682EC932C8376F41A29C4488D05C618A1CD852CF22EA2E914DA19AC598D3C7DE69DC8EDD742CB073496E121A2614BA8CFD85570B783127A0897F9AD15C97797C13A6BF5B324417404C97C5E66FC84F89EB3985DC33454C4803C1BC0B1ED1656349596477F55C205D07A42310575FE114DD613FF4002CBE2173F488D7910DCCEF3D5E21FBB98C00EA40DA07A2AEF6F1B98B5611F2638E51B6874FB061C77FFAE13F172D6DCD68540000, N'6.1.3-40302')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'ca3884d7-a1dd-46ae-9789-427677fa757e', N'Admin')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'7211157c-7248-4af3-a100-453adf8a3b01', N'Student')
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'4c0e515e-e5be-4368-95a1-1cee2844f63f', N'Teacher')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', N'4c0e515e-e5be-4368-95a1-1cee2844f63f')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'5ee35318-6d77-4815-af28-4e1397e89e06', N'7211157c-7248-4af3-a100-453adf8a3b01')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'7211157c-7248-4af3-a100-453adf8a3b01')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'cd0c0e87-ba64-4bd3-b89e-eecffc6bdc5b', N'7211157c-7248-4af3-a100-453adf8a3b01')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'47da9e10-0e53-4cb9-b600-6bea6dc3ddc9', N'ca3884d7-a1dd-46ae-9789-427677fa757e')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'47da9e10-0e53-4cb9-b600-6bea6dc3ddc9', N'carla@example.com', 0, N'AD9yuS2NJ2DJDMlI47j3Hpu02IXbgAQNUL9Th4/rNe+RMRjKir79MHCMeeDJRojviQ==', N'3e49f7c1-f8c9-48a3-b098-e886cc203422', NULL, 0, 0, NULL, 1, 0, N'carla@example.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'5ee35318-6d77-4815-af28-4e1397e89e06', N'aducklinc@none.com', 0, N'ANWwJnM9eVfTZvrVutv9kYNJ7WwHPEhQTFEPUu0FexrphAX/Jx9xhfwAKRukrQl2HQ==', N'67b440e1-35eb-4416-95ca-524767669746', NULL, 0, 0, NULL, 1, 0, N'aducklinc@none.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', N'teacher@none.com', 0, N'ADo9LoN2rt7KFvAc/TUJCmlRoesqxKrVa1r/CJKCsYbEx8C7at+FcY9qWrb1jor5Og==', N'7a29cdd7-9848-4691-bd0e-b574333328f1', NULL, 0, 0, NULL, 1, 0, N'teacher@none.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'mcduff@none.com', 0, N'AIUrW85BFoeqPjHlsF/dBF3mZuxPPhsDWltB8f/UgAtKIIc2QcykJnN4b6URVszwsw==', N'db1826bb-5bd3-4a6b-827d-74abe45e254d', NULL, 0, 0, NULL, 1, 0, N'mcduff@none.com')
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'cd0c0e87-ba64-4bd3-b89e-eecffc6bdc5b', N'imcduck@none.com', 0, N'APF2UKTXPHUL1tTIipYw6I66bIpYkSyCZfh5cm1ukW8nePivPpBVxRl9INe4ab3Gag==', N'a70f441f-37e1-43f9-bb45-003fd78b526f', NULL, 0, 0, NULL, 1, 0, N'imcduck@none.com')
SET IDENTITY_INSERT [dbo].[Locations] ON 

INSERT [dbo].[Locations] ([LocationID], [LocationName], [Address], [City], [State], [ZipCode], [ReservationLimit], [TeacherID], [DocRequirements], [Comments]) VALUES (1, N'CNA: Warrensburg Area Career Center', N'205 South Ridgeview Drive', N'Warrensburg', N'MO', N'64093', 1, N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', N'EDL showing no results.', N'Certified Nursing Assistant Class. ')
INSERT [dbo].[Locations] ([LocationID], [LocationName], [Address], [City], [State], [ZipCode], [ReservationLimit], [TeacherID], [DocRequirements], [Comments]) VALUES (2, N'RNA: The Groves', N'1515 White Oak Dr', N'Independence', N'MO', N'64050', 1, N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'EDL showing no results.', N'Restorative Nursing Assistant Class')
INSERT [dbo].[Locations] ([LocationID], [LocationName], [Address], [City], [State], [ZipCode], [ReservationLimit], [TeacherID], [DocRequirements], [Comments]) VALUES (4, N'CMT: Summit Tech Academy', N'1101 W Innovation Dr.', N'Lee''s Summit', N'MO', N'64086', 1, N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'EDL showing no results.', N'Certified Medication Technician Class')
SET IDENTITY_INSERT [dbo].[Locations] OFF
SET IDENTITY_INSERT [dbo].[Reservations] ON 

INSERT [dbo].[Reservations] ([ReservationID], [LocationID], [ReservationDate], [TeacherID], [StudentDocID]) VALUES (8, 1, CAST(0x7E3F0B00 AS Date), N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', 3)
INSERT [dbo].[Reservations] ([ReservationID], [LocationID], [ReservationDate], [TeacherID], [StudentDocID]) VALUES (13, 4, CAST(0x833F0B00 AS Date), N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', 3)
INSERT [dbo].[Reservations] ([ReservationID], [LocationID], [ReservationDate], [TeacherID], [StudentDocID]) VALUES (14, 2, CAST(0x703F0B00 AS Date), N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', 5)
INSERT [dbo].[Reservations] ([ReservationID], [LocationID], [ReservationDate], [TeacherID], [StudentDocID]) VALUES (15, 1, CAST(0x833F0B00 AS Date), N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', 4)
INSERT [dbo].[Reservations] ([ReservationID], [LocationID], [ReservationDate], [TeacherID], [StudentDocID]) VALUES (16, 4, CAST(0xF73F0B00 AS Date), NULL, 3)
SET IDENTITY_INSERT [dbo].[Reservations] OFF
SET IDENTITY_INSERT [dbo].[StudentDocs] ON 

INSERT [dbo].[StudentDocs] ([StudentDocID], [DocName], [StudentID], [StudentPhoto], [SpecialNotes], [IsActive], [DateAdded]) VALUES (3, N'EDL.pdf', N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'DuckyMcDufferson.jpg', NULL, 1, CAST(0x773F0B00 AS Date))
INSERT [dbo].[StudentDocs] ([StudentDocID], [DocName], [StudentID], [StudentPhoto], [SpecialNotes], [IsActive], [DateAdded]) VALUES (4, N'EDL.pdf', N'5ee35318-6d77-4815-af28-4e1397e89e06', N'AberduckLincoln.jpg', NULL, 1, CAST(0x773F0B00 AS Date))
INSERT [dbo].[StudentDocs] ([StudentDocID], [DocName], [StudentID], [StudentPhoto], [SpecialNotes], [IsActive], [DateAdded]) VALUES (5, N'EDL.pdf', N'cd0c0e87-ba64-4bd3-b89e-eecffc6bdc5b', N'IrishMcDuck.jpg', NULL, 1, CAST(0x773F0B00 AS Date))
SET IDENTITY_INSERT [dbo].[StudentDocs] OFF
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'47da9e10-0e53-4cb9-b600-6bea6dc3ddc9', N'Carla', N'Sapp')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'5ee35318-6d77-4815-af28-4e1397e89e06', N'Aberduck', N'Lincoln')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'941b5918-ca88-46cd-9cec-e1b7c2ac6a12', N'Sally', N'Teacher')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'991364c7-8dbe-4245-aa2f-55883c32f625', N'Centriq', N'Administrator')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'c9badd92-4fdc-44f3-9b33-c22d34510102', N'Ducky', N'McDufferson')
INSERT [dbo].[UserDetails] ([UserID], [FirstName], [LastName]) VALUES (N'cd0c0e87-ba64-4bd3-b89e-eecffc6bdc5b', N'Irish', N'McDuck')
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[DF_StudentDocs_DateAdded]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[StudentDocs] ADD  CONSTRAINT [DF_StudentDocs_DateAdded]  DEFAULT (getdate()) FOR [DateAdded]
END

GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserClaims]'))
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserLogins]'))
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]') AND parent_object_id = OBJECT_ID(N'[dbo].[AspNetUserRoles]'))
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations]  WITH CHECK ADD  CONSTRAINT [FK_Reservations_Locations] FOREIGN KEY([LocationID])
REFERENCES [dbo].[Locations] ([LocationID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_Locations]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations] CHECK CONSTRAINT [FK_Reservations_Locations]
GO
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_StudentDocs]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations]  WITH CHECK ADD  CONSTRAINT [FK_Reservations_StudentDocs] FOREIGN KEY([StudentDocID])
REFERENCES [dbo].[StudentDocs] ([StudentDocID])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Reservations_StudentDocs]') AND parent_object_id = OBJECT_ID(N'[dbo].[Reservations]'))
ALTER TABLE [dbo].[Reservations] CHECK CONSTRAINT [FK_Reservations_StudentDocs]
GO
